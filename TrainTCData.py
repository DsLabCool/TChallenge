
# coding: utf-8
import gc
import pandas as pd
from  OneHotEncode import encode
from read_csv_in_order import load_column_type
from sklearn import linear_model
import numpy as np
from sklearn.svm import SVR
from sklearn.metrics import mean_squared_error


data_file_path = 'TChallengeData/train_data.csv'
# data_file_path = 'MLTestData/CSVLoaderTest.csv'
column_type_path = 'column_types/tc_column_type.txt'
# column_type_path = 'column_types/csv_loader_test_type.txt'


df = pd.read_csv(data_file_path)

# del df
# gc.collect()

columns_name, columns_type, columns_encode_type = load_column_type(column_type_path)


matrix, dic, arranged_names = encode(df, columns_name, columns_type, columns_encode_type)
print(arranged_names)


label = 'invest'

train_X_columns = []
train_Y_columns = []

for i in range(len(arranged_names)):
    name = arranged_names[i]
    if name != label:
        train_X_columns.append(i)
    else:
        train_Y_columns.append(i)

X_train = matrix[:, train_X_columns]


y_train = matrix[:, train_Y_columns].ravel()
print(y_train)


# clf = linear_model.SGDRegressor()
# clf.fit(X_train, np.log(y_train))
#
# results = np.exp(clf.predict(X_train))



clf = SVR(C=0.0001, cache_size=200, coef0=0.0, degree=3, epsilon=0.2, gamma=0.0001,
    kernel='rbf', max_iter=-1, shrinking=True, tol=0.00001, verbose=False)
clf.fit(X_train, y_train)

y_pred = clf.predict(X_train)



mse = mean_squared_error(y_train, y_pred)

print(mse)



