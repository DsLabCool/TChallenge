from sklearn.svm import SVR
import features
import numpy as np
from CSVToNumpy import csv_to_data_frame
from CSVToNumpy import load_column_type
from sklearn.metrics import mean_squared_error
from sklearn.preprocessing import StandardScaler
import re
import time

def data_frame_decomposition(data_frame, label_index):
    numpy_matrix = data_frame.as_matrix()

    (row, column) = numpy_matrix.shape

    feature_index = list(range(0, column))
    feature_index.remove(label_index)

    X = numpy_matrix[:, feature_index]
    y = numpy_matrix[:, label_index]

    return X, y


def svr_train(X, y):

    st = time.time()

    X = StandardScaler().fit(X)


    print(X)
    print(y)

    clf = SVR(C=0.0001, epsilon=0.1, gamma=0.001, tol=0.001)
    clf.fit(X, y)

    et = time.time()

    print('spend time: ' + str(et - st) + 's')

    print(clf)

    return clf

# y = np.random.randn(n_samples)
# X = np.random.randn(n_samples, n_features)


def svr_validation(clf, X, y):
    predict_res = clf.predict(X)
    print('y:'+str(y)+'\npredict:'+str(predict_res))
    mse = mean_squared_error(y, predict_res)
    print('mse: '+str(mse))
    return mse

if __name__ == '__main__':
    file_path = 'column_types/tc_column_type.txt'
    column_names, column_type = load_column_type(file_path)

    csv_columns_type = []

    for i in range(len(column_names)):
        csv_columns_type.append(column_type[column_names[i]])

    # print(csv_columns_type)

    # csv_columns_type = [features.NOMINAL, features.NOMINAL, features.NOMINAL, features.NOMINAL,
    #                     features.NUMERIC, features.NUMERIC, features.NOMINAL]

    # df = csv_to_data_frame('MLTestData/ml/CSVLoaderTest.csv', True, csv_columns_type)
    df = csv_to_data_frame('TChallengeData/train_10_data.csv', True, csv_columns_type)

    X, y = data_frame_decomposition(df, 1)



    clf = svr_train(X, y)
    svr_validation(clf, X, y)