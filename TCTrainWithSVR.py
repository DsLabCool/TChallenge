
# coding: utf-8

# In[8]:

from CSVToNumpy import read_train_csv
from CSVToNumpy import load_column_type
from CSVToNumpy import read_test_csv_to_matrix
from sklearn.svm import SVR
import numpy as np
from sklearn.metrics import mean_squared_error
import time


# In[ ]:

st = time.time()
type_file_path = 'column_types/tc_column_type.txt'
# type_file_path = 'column_types/csv_loader_test_type.txt'

# csv_file_path = 'TChallengeData/train_1000_data.csv'
csv_file_path = 'TChallengeData/train_data.csv'
# csv_file_path = 'TestData/ml/CSVLoaderTest.csv'
# csv_to_data_frame(csv_file_path, True, csv_columns_type)

df, cnt = read_train_csv(csv_file_path, type_file_path)

et = time.time()

print('spend' + (et - st)+'s')

print(df)

# print(df)
# df


# In[11]:

type_file_path = 'column_types/tc_column_type.txt'
columns_name, columns_type, columns_encode_type = load_column_type(type_file_path)

column_num = len(df.columns)

feature_index = list(range(0, column_num))
label_index = 1
feature_index.remove(label_index)

df_X = df.iloc[:, feature_index]
# X
label = columns_name[label_index]
y = df[label]
print(y)
# y


# In[12]:

def encode_mapping(df_X, columns_name, columns_type, columns_encode_type):
    tem_list = []
    for index, row in df_X.iterrows():
        line_list = []
        for i in range(len(columns_name)):
            column_name = columns_name[i]
            # print(row[column_name])

            if columns_type[column_name] == 2 and columns_encode_type[column_name] == 2:
                # print(column_name)
                # print(row[column_name])
                line_list += map(int, list(row[column_name]))
            else:
                line_list += [row[column_name]]
        tem_list.append(line_list)
    # print(tem_list)
    X_train = np.array(tem_list)
    # print(X_train)
    return X_train


# In[13]:

columns_name_exclude_label = columns_name.copy()
del columns_name_exclude_label[label_index]
X_train = encode_mapping(df_X, columns_name_exclude_label, columns_type, columns_encode_type)
print(X_train)
# X_train


# In[14]:

clf = SVR(C=0.0001, epsilon=0.00001, gamma=16, tol=0.000001)
clf.fit(X_train, y)


# In[15]:

test_type_file_path = 'column_types/test_tc_column_type.txt'
# test_csv_file_path = 'TChallengeData/test_10_data.csv';
test_csv_file_path = 'TChallengeData/test_data_for_m6.csv';

test_X = read_test_csv_to_matrix(test_csv_file_path, test_type_file_path, cnt)
print(test_X)
# test_X


# In[16]:

predict_res = clf.predict(test_X)
print('y:'+str(y)+'\npredict:'+str(predict_res))
mse = mean_squared_error(y, predict_res)
print('mse: '+str(mse))



