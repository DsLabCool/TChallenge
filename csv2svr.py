# -*- coding: utf-8 -*-
import pandas as pd
import numpy as np
from sklearn.svm import SVR
from sklearn.metrics import mean_squared_error


def csv2svr(csvpath, cate_idx, label_y):
    df = pd.read_csv(csvpath)

    header = list(df.columns.values)
    # print(header)
    for idx in cate_idx:
        mapping = dict()
        rvs_mapping = dict()
        num_idx = 1

        col = df.iloc[:, idx]
        for data in col:
            if data not in mapping:
                mapping[data] = num_idx
                rvs_mapping[num_idx] = data
                num_idx += 1

        df[header[idx]] = df[header[idx]].map(mapping)

    print(df)

    numpy_matrix = df.as_matrix()

    # print(numpy_matrix)

    # X = numpy_matrix[:, :-1]
    X = np.delete(numpy_matrix, label_y, axis=1)
    y = numpy_matrix[:, label_y]

    print('X:'+str(X))
    print('y:'+str(y))

    clf = SVR(C=10.0, epsilon=0.0001, gamma=0.001, tol=0.001)
    # SVR(C=100.0, cache_size=200, coef0=0.0, degree=3, epsilon=0.2, gamma='auto',
    #     kernel='rbf', max_iter=-1, shrinking=True, tol=0.001, verbose=False)

    clf.fit(X, y)

    res = clf.predict(X)

    print('res: '+str(res))

    print(y-res)

    print('mse: '+str(mean_squared_error(y, res)))

    # y = df['class label'].values
    # X = df.iloc[:, :-1].values
    # X = np.apply_along_axis(func1d= lambda x: np.array(list(x[0]) + list(x[1:])), axis=1, arr=X)

    # print('Class labels:', y)
    # print('\nFeatures:\n', X)

if __name__ == '__main__':
    path = 'MLTestData/ml/CSVLoaderTest.csv'
    csv2svr(path, (1, 2, 6), 6)
