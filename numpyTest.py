import numpy as np


X = np.random.rand(3, 2)

rows, cols = X.shape

print(X)

max_values = np.amax(X, axis=0)
min_values = np.amin(X, axis=0)

for col in range(cols):
    mat = [(X[:, col][i]-min_values[col])/(max_values[col]-min_values[col]) for i in range(len(X[:, col]))]
    print(mat)
    X[:, col] = mat

print(X)
