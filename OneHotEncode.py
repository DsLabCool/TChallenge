
# coding: utf-8

import pandas as pd
from sklearn.preprocessing import OneHotEncoder
from read_csv_in_order import load_column_type
import types
import numpy as np
from read_csv_in_order import load_column_type


def encode(df, columns_name, columns_type, columns_encode_type):
    arranged_column_name, numeric_columns_name, category_number_columns_name, category_binary_columns_name = \
            _get_arrange_columns(columns_name, columns_type, columns_encode_type)
    dic = _get_column_encode_mapping(df, category_number_columns_name, category_binary_columns_name)

    matrix = _do_encode(df, numeric_columns_name, category_number_columns_name, category_binary_columns_name, dic)
    return matrix, dic, arranged_column_name


def _get_arrange_columns(columns_name, columns_type, columns_encode_type):
    numeric_columns_name = []
    category_number_columns_name = []
    category_binary_columns_name = []

    for name in columns_name:
        if columns_type[name] == 1:
            numeric_columns_name.append(name)
        elif columns_type[name] == 2 and columns_encode_type[name] == 1:
            category_number_columns_name.append(name)
        else:
            category_binary_columns_name.append(name)

    arranged_column_name = numeric_columns_name + category_number_columns_name + category_binary_columns_name
    # print(arranged_column_name)

    # print(numeric_columns_name)
    # print(category_number_columns_name)
    # print(category_binary_columns_name)
    return arranged_column_name, numeric_columns_name, category_number_columns_name, category_binary_columns_name


def _get_column_encode_mapping(df, category_number_columns_name, category_binary_columns_name):
    dic = {}
    for name in category_number_columns_name:
        names = pd.Series(df[name]).astype('category').values
        categories = names.categories
        # print(categories)
        serial_num = 0
        category_dic = {}
        for category in categories:
            serial_num += 1
            category_dic[category] = serial_num
            #         print(str(serial_num) +' '+ category)
        dic[name] = category_dic

    for name in category_binary_columns_name:
        names = pd.Series(df[name]).astype('category').values
        categories = names.categories
        # print(categories)
        serial_num = 0
        category_dic = {}
        for category in categories:
            serial_num += 1
            category_dic[category] = serial_num
            #         print(str(serial_num) +' '+ category)
        dic[name] = category_dic
    # print(dic)
    return dic


def _do_encode(df, numeric_columns_name, category_number_columns_name, category_binary_columns_name, dic, fillna_value=0):
    # print(df)

    for name in category_number_columns_name:
        tem_dic = dic[name]
        # print(tem_dic)
        df[name] = df[name].map(tem_dic)

    for name in category_binary_columns_name:
        tem_dic = dic[name]
        # print(tem_dic)
        df[name] = df[name].map(tem_dic)
    df = df.fillna(fillna_value)

    start_matrix = df[numeric_columns_name].as_matrix()
    middle_matrix = df[category_number_columns_name].as_matrix()

    enc = OneHotEncoder()
    # print(category_binary_columns_name)
    end_matrix = df[category_binary_columns_name].as_matrix()

    # type(sec_matrix)

    enc.fit(end_matrix)
    enc_end_matrix = enc.transform(end_matrix).toarray()
    # print(enc_end_matrix)
    # type(enc_sec_matrix)
    matrix = np.append(np.append(start_matrix, middle_matrix, axis=1), enc_end_matrix, axis=1)
    # print(matrix)
    return matrix


if __name__ == '__main__':


    # data_file_path = 'MLTestData/CSVLoaderTest.csv'
    data_file_path = 'TChallengeData/train_1000_data.csv'
    # column_type_path = 'column_types/csv_loader_test_type.txt'
    column_type_path = 'column_types/tc_column_type.txt'


    df = pd.read_csv(data_file_path)
    columns_name, columns_type, columns_encode_type = load_column_type(column_type_path)

    matrix, dic, arranged_column_name = encode(df, columns_name, columns_type, columns_encode_type)

    print(matrix)
    print(dic)
    print(arranged_column_name)