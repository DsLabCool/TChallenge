

n = 10000

content = list()

filepath = '../TChallengeData/train_data.csv'
Wfilepath = '../TChallengeData/train_'+str(n)+'_data.csv'

header = ""
num_lines = 0

with open(filepath) as rf:
    with open(Wfilepath, 'w') as wf:
        for line in rf:
            line = line.strip()
            num_lines += 1

            if num_lines == 1:
                header = line.strip()
            else:
                content.append(line)

            if num_lines % 100000 == 0:
                print(num_lines)

            # print(line)
            wf.write(line+'\n')

            if num_lines > n:
                break

