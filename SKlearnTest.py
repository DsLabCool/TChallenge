import numpy as np
from sklearn.cross_validation import train_test_split
n_samples, n_features = 10, 5
np.random.seed(0)
y = np.random.randn(n_samples)
X = np.random.randn(n_samples, n_features)


X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)


print(X)

ylist = list(y)

print(ylist)
print("---------------")
print(X_train)

print(y_train)