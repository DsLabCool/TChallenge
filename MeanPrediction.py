with open('TChallengeData/train_data.csv') as rf:
    next(rf)
    count = 0
    sum_invest = 0
    for line in rf:
        line = line.strip()
        invest = float(line.split(',')[1])
        sum_invest += invest
        count += 1

        if count % 100000 == 0:
            print(count)

    mean_invest = sum_invest/count
    print(mean_invest)

with open('TChallengeData/test_data_for_m6.csv') as rf:
    with open('results/prediction_mean_mse_2.452374.csv', 'w') as wf:
        header = next(rf)
        count = 0
        for line in rf:
            wf.write(str(mean_invest)+'\n')
            count+=1
            # print(line.strip())

    print(count)

