import datetime
from numpy import genfromtxt
from sklearn import preprocessing
import pandas as pd
import numpy as np
import re

# st = datetime.datetime.now()
# my_data = genfromtxt('MLTestData/ml/CSVLoaderTest.csv', delimiter=',')
#
# et = datetime.datetime.now()
#
#
# print(my_data)

def read_train_csv(train_file_path, train_type_path, has_header=True, fill_NaN=True):
    columns_name, columns_type, columns_encode_type = load_column_type(train_type_path)
    columns_nominal_types, content = _load_train_data_content(train_file_path, has_header, columns_name, columns_type, fill_NaN, columns_encode_type)
    data_frame = _create_data_frame(columns_name, columns_nominal_types, content)
    return data_frame, columns_nominal_types


def read_test_csv_to_matrix(test_file_path, test_type_path, columns_nominal_types, has_header=True, fill_NaN=True):
    columns_name, columns_type, columns_encode_type = load_column_type(test_type_path)
    content = _load_test_data_content(test_file_path, columns_name, columns_type, columns_nominal_types, has_header, fill_NaN)
    return np.array(content)

    # data_frame = _create_data_frame(columns_name, columns_nominal_types, content)
    # return data_frame, columns_nominal_types

def load_column_type(_train_file_path):
    columns_name = []
    columns_type = {}
    columns_encode_type = {}
    with open(_train_file_path) as rf:

        for line in rf:
            line = line.strip()

            math_obj = re.match('#.*', line)
            if not math_obj:
                line_split = line.split('=')
                column_name = line_split[0]
                columns_name.append(column_name)
                columns_type[column_name] = int(line_split[1])
                if int(line_split[1]) == 2:
                    columns_encode_type[column_name] = int(line_split[2])


    # print(column_names)
    # print(column_type)
    return columns_name, columns_type, columns_encode_type


def _load_train_data_content(file_path, has_header, columns_name, columns_type, fill_NaN, columns_encode_type):
    _columns_nominal_types = {}
    _content = list()

    with open(file_path) as rf:
        if has_header:
            header = next(rf).strip()
            _parse_header(header, columns_type, _columns_nominal_types)
            # print(header)

        count = 0
        for line in rf:
            count +=1
            if count % 100000 == 0:
                print(count)

            line = line.strip()
            _parse_train_line_of_content(line, columns_name, columns_type, _columns_nominal_types, _content, fill_NaN, columns_encode_type)
            # print(line)

    # for line_of_content in content:
    #     print(line_of_content)CSVToNumpy.py:59

    for column_name in _columns_nominal_types:
        encode = columns_encode_type[column_name]

        if encode == encoder().NUMBER:
            nominal_types = _columns_nominal_types[column_name]
            nominal_types['unknown'] = len(nominal_types)+1

        if encode == encoder().BINARY:
            nominal_types = _columns_nominal_types[column_name]
            nominal_types['unknown'] = "{0:b}".format(1 << len(nominal_types))

            for nominal_type in nominal_types:
                nominal_types[nominal_type] = tuple(nominal_types[nominal_type].rjust(len(nominal_types), '0'))

            # print(nominal_types)

    # print(_columns_nominal_types)

    return _columns_nominal_types, _content

def _load_test_data_content(test_file_path, columns_name, columns_type, columns_nominal_types, has_header, fill_NaN):

    _content = list()

    with open(test_file_path) as rf:
        if has_header:
            header = next(rf).strip()

        count = 0
        for line in rf:
            count += 1
            if count % 100000 == 0:
                print(count)

            line = line.strip()
            _parse_test_line_of_content(line, columns_name, columns_type, columns_nominal_types, _content, fill_NaN)
    return _content

def _create_data_frame(_columns_name, _columns_nominal_types, _content):

    data_frame = pd.DataFrame(_content)
    data_frame.columns = _columns_name

    nominal_columns_name = _columns_nominal_types.keys()

    for column_name in nominal_columns_name:
        data_frame[column_name] = data_frame[column_name].map(_columns_nominal_types[column_name])

    # print(data_frame)
    return data_frame


def _parse_header(header, columns_type, columns_nominal_types):
    header_split = header.split(',')

    for i in range(len(header_split)):

        column_name = header_split[i]
        column_type = columns_type[column_name]
        if column_type == features().NOMINAL:
            columns_nominal_types[column_name] = {}

    # print(columns_nominal_types)
    # print(header_split)


def _parse_train_line_of_content(line, column_names, columns_type, columns_nominal_types, content, fill_NaN, columns_encode_type):
    line_split = line.split(',')
    line_of_content = []
    for i in range(len(line_split)):
        record = line_split[i]
        column_name = column_names[i]
        column_type = columns_type[column_name]

        if column_type == features().NUMERIC:

            if not record:
                if fill_NaN:
                    record = '0'
                else:
                    record = 'NaN'
                # print('empty'+record)

            line_of_content.append(float(record))

        elif column_type == features().NOMINAL:

            encode = columns_encode_type[column_name]

            if not record:
                if fill_NaN:
                    record = 'null'
                else:
                    record = 'NaN'
                # print('empty'+record)

            line_of_content.append(record)
            nominal_types = columns_nominal_types[column_names[i]]  # nominal type dict of one column

            if record not in nominal_types:
                nominal_types_size = len(nominal_types)

                if encode == encoder().NUMBER:
                    nominal_types[record] = nominal_types_size+1
                elif encode == encoder().BINARY:
                    nominal_types[record] = "{0:b}".format(1 << nominal_types_size)
    content.append(line_of_content)


def _parse_test_line_of_content(line, columns_name, columns_type, columns_nominal_types, content, fill_NaN):
    line_split = line.split(',')
    line_of_content = []

    for i in range(len(columns_name)):
        # print('column: '+str(i))
        column_name = columns_name[i]
        record = line_split[i]
        column_type = columns_type[column_name]

        if column_type == features().NUMERIC:
            if not record:
                if fill_NaN:
                    record = '0'
                else:
                    record = 'NaN'
                # print('empty'+record)
            print(float(record), end=',')
            line_of_content.append(float(record))

        elif column_type == features().NOMINAL:
            if not record:
                if fill_NaN:
                    record = 'null'
                else:
                    record = 'NaN'
                # print('empty'+record)

            encode_value = ""
            if record in columns_nominal_types[column_name]:
                encode_value = columns_nominal_types[column_name][record]

                if isinstance(encode_value, tuple):
                    # print([int(j) for j in list(encode_value)], end=',')
                    line_of_content += map(int, list(encode_value))
                else:
                    line_of_content.append(encode_value)
                    # print(encode_value, end=',')
            else:
                encode_value = columns_nominal_types[column_name]['unknown']
                if isinstance(encode_value, tuple):
                    # print([int(j) for j in list(encode_value)], end=',')
                    line_of_content += map(int, list(encode_value))
                else:
                    line_of_content.append(encode_value)
                    # print(encode_value, end=',')
    print()


    content.append(line_of_content)


class features:
    NUMERIC = 1
    NOMINAL = 2


class encoder:
    NUMBER = 1
    BINARY = 2


if __name__ == '__main__':

    type_file_path = 'column_types/tc_column_type.txt'
    # type_file_path = 'column_types/csv_loader_test_type.txt'
    # column_names, column_type = load_column_type(type_file_path)
    #
    # csv_columns_type = []
    #
    # for i in range(len(column_names)):
    #     csv_columns_type.append(column_type[column_names[i]])
    #
    csv_file_path = 'TChallengeData/train_data.csv'
    # csv_file_path = 'MLTestData/CSVLoaderTest.csv'
    # csv_to_data_frame(csv_file_path, True, csv_columns_type)

    df, cnt = read_train_csv(csv_file_path, type_file_path)
    print(df)
    print(cnt)

    test_type_file_path = 'column_types/test_tc_column_type.txt'
    test_csv_file_path = 'TChallengeData/test_data_for_m6.csv';

    test_X = read_test_csv_to_matrix(test_csv_file_path, test_type_file_path, cnt)
    print(test_X)