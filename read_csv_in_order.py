import pandas as pd
import re
import time


def create_order_data_csv(column_type_path, data_file_path):
    columns_name, columns_type, columns_encode_type = load_column_type(column_type_path)
    order_df = _transform_data_frame(data_file_path, columns_name, columns_type, columns_encode_type)
    _output_order_data_frame(data_file_path, order_df)


def load_column_type(column_type_path):
    columns_name = []
    columns_type = {}
    columns_encode_type = {}
    with open(column_type_path) as rf:

        for line in rf:
            line = line.strip()

            math_obj = re.match('#.*', line)
            if not math_obj:
                line_split = line.split('=')
                column_name = line_split[0]
                columns_name.append(column_name)
                columns_type[column_name] = int(line_split[1])
                if int(line_split[1]) == 2:
                    columns_encode_type[column_name] = int(line_split[2])

    # print(column_names)
    # print(column_type)
    return columns_name, columns_type, columns_encode_type


def _transform_data_frame(data_file_path, columns_name, columns_type, columns_encode_type):

    st = time.time()
    df = pd.read_csv(data_file_path)
    et = time.time()
    print('read origin data spend: '+str(et - st) + 's')

    first_part = []
    second_part = []

    for column in columns_name:
        if columns_type[column] == 2 and columns_encode_type[column] == 2:
            second_part.append(column)
        else:
            first_part.append(column)

    print(first_part)
    print(second_part)

    columns_name = first_part + second_part

    order_df = df[columns_name]

    return order_df


def _output_order_data_frame(data_file_path, order_df):
    split_data_file_path = data_file_path.split('.')

    new_file_path = ''
    for i in range(len(split_data_file_path) - 1):
        new_file_path += split_data_file_path[i]
    new_file_path += '_InOrder.' + split_data_file_path[len(split_data_file_path) - 1]
    print(new_file_path)
    print(order_df)

    order_df.to_csv(new_file_path, sep=',', encoding='utf-8', index=False)



# with open('TChallengeData/train_data_InOrder.csv', 'w') as wf:
#     for name in df.columns:
#         wf.write(name)
#         if name != df.columns[len(df.columns)-1]:
#             wf.write(',')
#     wf.write('\n')
#     for index, row in df.iterrows():
#         line = ""
#         for column in columns_name:
#             line += str(row[column])
#             if column != columns_name[len(columns_name)-1]:
#                 line += ','
#         print(line)
#         wf.write(line+'\n')

if __name__ == '__main__':

    data_file_path = 'TChallengeData/train_10_data.csv'
    # data_file_path = 'MLTestData/CSVLoaderTest.csv'
    column_type_path = 'column_types/tc_column_type.txt'
    # column_type_path = 'column_types/csv_loader_test_type.txt'

    create_order_data_csv(column_type_path, data_file_path)

