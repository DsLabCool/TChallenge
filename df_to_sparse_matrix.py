
# coding: utf-8

import pandas as pd
import re
import pickle
import numpy as np
from scipy import sparse
import time
import math


def output_train_num_data_frame(data_path, num_data_path, column_type_path, categories_pickle_path, chunksize=10000):
    columns_name, columns_type, columns_encode_type = load_column_type(column_type_path)
    num_df = trans_to_train_num_data_frame(data_path, columns_name, columns_type, categories_pickle_path)
    output(num_data_path, num_df, chunksize)


def output_train_sparse_matrix(num_data_path, columns_name, columns_type, columns_encode_type, categories_pickle_path, label,
                               train_X_sm_pickle_path, train_y_pickle_path, chunk_size):
    trans_to_sparse(num_data_path, columns_name, columns_type, columns_encode_type, categories_pickle_path, label,
                         train_X_sm_pickle_path, train_y_pickle_path, chunk_size)


def output_test_num_data_frame(data_path, num_data_path, column_type_path, categories_pickle_path, chunksize=10000):
    columns_name, columns_type, columns_encode_type = load_column_type(column_type_path)
    num_df = trans_to_test_num_data_frame(data_path, columns_name, columns_type, categories_pickle_path)
    output(num_data_path, num_df, chunksize)


def output_test_sparse_matrix(num_data_path, columns_name, columns_type, columns_encode_type, categories_pickle_path, label,
                              test_X_sm_pickle_path, test_y_pickle_path, chunk_size):
    trans_to_sparse(num_data_path, columns_name, columns_type, columns_encode_type, categories_pickle_path, label,
                    test_X_sm_pickle_path, test_y_pickle_path, chunk_size)


def load_column_type(column_type_path):
    # print('load column type')

    columns_name = []
    columns_type = {}
    columns_encode_type = {}
    with open(column_type_path) as rf:

        for line in rf:
            line = line.strip()

            math_obj = re.match('#.*', line)
            if not math_obj:
                line_split = line.split('=')
                column_name = line_split[0]
                columns_name.append(column_name)
                columns_type[column_name] = int(line_split[1])
                if int(line_split[1]) == 2:
                    columns_encode_type[column_name] = int(line_split[2])
    return columns_name, columns_type, columns_encode_type


def trans_to_train_num_data_frame(data_path, columns_name, columns_type, categories_pickle_path):

    print('trans to num data frame')

    df = pd.read_csv(data_path)

    print('create dictionary')
    dic = {}
    for name in columns_name:
        print(name)
        if columns_type[name] == 2:
            #         print('category')
            categories = pd.Series(df[name]).astype('category').values.categories
            serial_num = 0
            category_dic = {}
            for category in categories:
                serial_num += 1
                category_dic[category] = serial_num
            category_dic['unknown'] = serial_num + 1
            dic[name] = category_dic
    with open(categories_pickle_path, 'wb') as wf:
        pickle.dump(dic, wf)

    print('data frame mapping')
    for name in columns_name:
        print(name)
        if columns_type[name] == 2:
            tem_dic = dic[name]
            # print(tem_dic)
            df[name] = df[name].map(tem_dic)

    df = df.fillna(int(0))
    return df

def trans_to_test_num_data_frame(data_path, columns_name, columns_type, categories_pickle_path):

    print('trans to num data frame')

    df = pd.read_csv(data_path)

    print('load dictionary')

    with open(categories_pickle_path, 'rb') as rf:
        dic = pickle.load(rf)

    print('data frame mapping')
    for name in columns_name:
        print(name)
        if columns_type[name] == 2:
            tem_dic = dic[name]
            # print(tem_dic)
            df[name] = df[name].map(tem_dic)

    df = df.fillna(int(0))
    return df


def output(num_data_path, num_df, chunksize):
    print('output data')
    num_df.to_csv(num_data_path, index=False, chunksize=chunksize)


def trans_to_sparse(num_data_path, columns_name, columns_type, columns_encode_type, categories_pickle_path, label,
                    X_sm_pickle_path, y_pickle_path, chunk_size):

    st = time.time()
    with open(categories_pickle_path, 'rb') as rf:
        dic = pickle.load(rf)

    et = time.time()
    print('load dic pickle spend: ' + str(et - st) + ' s')

    st = time.time()
    num_lines = sum(1 for line in open(num_data_path)) -1
    et = time.time()
    print('count total line spend: ' + str(et - st) + ' s')

    iter = math.ceil(num_lines / chunk_size)

    start_pos = 0

    for i in range(iter):
        start_pos = process(i, columns_name, columns_type, columns_encode_type, num_data_path, dic, start_pos, label, X_sm_pickle_path, y_pickle_path, chunk_size)


def process(iter_time, columns_name, columns_type, columns_encode_type, num_data_path, dic, start_pos, label, train_X_sm_pickle_path, train_y_pickle_path, chunk_size):

    sm_rows = []
    sm_columns = []
    sm_data = []

    print('process# '+str(iter_time))

    y = []

    row = 0

    with open(num_data_path) as rf:

        rf.seek(start_pos)

        if iter_time == 0:
            header = rf.readline().strip()
        for i in range(chunk_size):
            line = rf.readline().strip()

            if line == '':
                break

            line_split = line.split(',')

            column_index = 0
            sm_column_cursor_index = 0

            column_len = 0

            for name in columns_name:
                if name != label:
                    if columns_type[name] == 2 and columns_encode_type[name] == 2:

                        bits_num = len(dic[name])

                        column_len += bits_num

                        record = int(float(line_split[column_index]))
                        shift = bits_num - record

                        sm_rows.append(i)
                        sm_columns.append(sm_column_cursor_index + shift)
                        sm_data.append(1)

                        sm_column_cursor_index += bits_num

                    else:

                        column_len += 1

                        sm_rows.append(i)
                        sm_columns.append(sm_column_cursor_index)

                        # print(line_split[column_index])

                        sm_data.append(float(line_split[column_index]))

                        # line_list += [float(line_split[column_index])]
                        sm_column_cursor_index += 1
                else:
                    y.append(float(line_split[column_index]))

                column_index += 1
            row += 1

            # if i % 10000 == 0:
            #     print(i)

            # print(line)

        X_sm = sparse.coo_matrix((np.array(sm_data), (np.array(sm_rows), np.array(sm_columns))), shape=(row, column_len))

        written_train_X_sm_pickle_path = train_X_sm_pickle_path.replace('.pickle', '_'+str(iter_time)+'.pickle')
        written_train_y_pickle_path = train_y_pickle_path.replace('.pickle', '_' + str(iter_time) + '.pickle')

        with open(written_train_X_sm_pickle_path, 'wb') as wf:
            pickle.dump(X_sm, wf)

        with open(written_train_y_pickle_path, 'wb') as wf:
            pickle.dump(y, wf)

        end_pos = rf.tell()

    return end_pos

if __name__ == '__main__':

    # data_path = 'MLTestData/CSVLoaderTest.csv'
    # data_path = 'TChallengeData/train_train_data.csv'
    # data_path = 'TChallengeData/train_data.csv'
    # data_path = 'TChallengeData/train_test_data.csv'
    # data_path = 'TChallengeData/test_data_for_m6.csv'
    # data_path = 'TChallengeData/train_data_no_id.csv'
    # data_path = 'TChallengeData/test_data_no_id.csv'
    # data_path = 'TChallengeData/train_data_no_id_productid.csv'
    # data_path = 'TChallengeData/test_data_no_id_productid.csv'
    # data_path = 'TChallengeData/train_data_no_id_productid_timestamp.csv'
    # data_path = 'TChallengeData/test_data_no_id_productid_timestamp.csv'
    # data_path = 'TChallengeData/train_1000_data_no_id_productid_train.csv'
    data_path = 'TChallengeData/train_1000_data_no_id_productid_test.csv'

    # column_type_path = 'column_types/csv_loader_test_type.txt'
    # column_type_path = 'column_types/tc_column_type.txt'
    # column_type_path = 'column_types/tc_column_type_no_id.txt'
    # column_type_path = 'column_types/test_tc_column_type.txt'
    column_type_path = 'column_types/tc_column_type_no_id_productid.txt'
    # column_type_path = 'column_types/test_tc_column_type_no_id_productid.txt'
    # column_type_path = 'column_types/tc_column_type_no_id_productid_timestamp.txt'
    # column_type_path = 'column_types/test_tc_column_type_no_id_productid_timestamp.txt'

    data_path_split = data_path.split('.')
    num_data_path = data_path_split[0] + '_Num' + '.' + data_path_split[1]

    data_path_split = data_path.split('/')
    categories_pickle_path = 'pickles/category_' + data_path_split[len(data_path_split) - 1].replace('.csv', '.pickle')


    # output_train_num_data_frame(data_path, num_data_path, column_type_path, categories_pickle_path)

    # columns_name, columns_type, columns_encode_type = load_column_type(column_type_path)
    # train_X_sm_pickle_path = 'pickles/train/' + data_path_split[len(data_path_split) - 1].replace('.csv', '_X_SM.pickle')
    # train_y_pickle_path = 'pickles/train/' + data_path_split[len(data_path_split) - 1].replace('.csv', '_y.pickle')
    #
    # chunk_size = 100000
    #
    # output_train_sparse_matrix(num_data_path, columns_name, columns_type, columns_encode_type, categories_pickle_path, 'invest',
    #                                train_X_sm_pickle_path, train_y_pickle_path, chunk_size)


    # test_categories_pickle_path = 'pickles/category_train_data.pickle'
    # test_categories_pickle_path = 'pickles/category_train_data_no_id.pickle'
    # test_categories_pickle_path = 'pickles/category_train_data_no_id_productid.pickle'
    # test_categories_pickle_path = 'pickles/category_train_data_no_id_productid_timestamp.pickle'
    test_categories_pickle_path = 'pickles/category_train_1000_data_no_id_productid_train.pickle'

    # output_test_num_data_frame(data_path, num_data_path, column_type_path, test_categories_pickle_path, chunksize=10000)


    columns_name, columns_type, columns_encode_type = load_column_type(column_type_path)

    test_X_sm_pickle_path = 'pickles/test/' + data_path_split[len(data_path_split) - 1].replace('.csv', '_X_SM.pickle')
    test_y_pickle_path = 'pickles/test/' + data_path_split[len(data_path_split) - 1].replace('.csv', '_y.pickle')

    chunk_size = 10000000

    output_test_sparse_matrix(num_data_path, columns_name, columns_type, columns_encode_type, test_categories_pickle_path, 'invest',
                              test_X_sm_pickle_path, test_y_pickle_path, chunk_size)

