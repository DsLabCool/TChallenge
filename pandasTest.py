import pandas as pd
import numpy as np
from sklearn.svm import SVR
from sklearn.metrics import mean_squared_error

content = list()

content.append(['green', 1, 10.1, 0])
content.append(['red', 2, 13.5, 1])
content.append(['blue', 3, 15.3, 0])


# df = pd.DataFrame(content)

df = pd.read_csv('MLTestData/ml/CSVLoaderTest.csv')

df.columns = ['id', 'name', 'sex', 'phone', 'height', 'weight', 'success']

name_mapping = {
           'Brian': 1,
           'Rock': 2,
           'Jack': 3}

sex_mapping = {
           'male': 1,
           'female': 2}

success_mapping = {
           'yes': 1,
           'no': 2}

df['name'] = df['name'].map(name_mapping)
df['sex'] = df['sex'].map(sex_mapping)
df['success'] = df['success'].map(success_mapping)

print(df)

numpy_matrix = df.as_matrix()

print(numpy_matrix)


X = numpy_matrix[:, [0, 1, 2]]
y = numpy_matrix[:, -1]

print('X:'+str(X))
print('y:'+str(y))

clf = SVR(C=1000.0, epsilon=0.000001, gamma=0.6, tol=0.0001)
# SVR(C=100.0, cache_size=200, coef0=0.0, degree=3, epsilon=0.2, gamma='auto',
#     kernel='rbf', max_iter=-1, shrinking=True, tol=0.001, verbose=False)

clf.fit(X, y)


res = clf.predict(X)

print('res: '+str(res))

print(y-res)

print('mse: '+str(mean_squared_error(y, res)))

# y = df['class label'].values
# X = df.iloc[:, :-1].values
# X = np.apply_along_axis(func1d= lambda x: np.array(list(x[0]) + list(x[1:])), axis=1, arr=X)

# print('Class labels:', y)
# print('\nFeatures:\n', X)