import datetime
from numpy import genfromtxt
from sklearn.metrics import mean_squared_error

from CSVToNumpy import csv_to_data_frame
import features

from sklearn.svm import SVR
import numpy as np

import pandas as pd

n_samples, n_features = 10, 5
np.random.seed(0)
y = np.random.randn(n_samples)
X = np.random.randn(n_samples, n_features)
clf = SVR(C=1000.0, epsilon=0.1, gamma=0.6, tol=0.0001)
# SVR(C=100.0, cache_size=200, coef0=0.0, degree=3, epsilon=0.2, gamma='auto',
#     kernel='rbf', max_iter=-1, shrinking=True, tol=0.001, verbose=False)

clf.fit(X, y)

print(clf)

print("y:"+str(y))

res = clf.predict(X)

print(res)

print(y-res)

print(mean_squared_error(y, res))

# st = datetime.datetime.now()
# my_data = genfromtxt('TChallengeData/train_data.csv', delimiter=',')
#
# et = datetime.datetime.now()

# print(et-st)

csv_columns_type = [features.NOMINAL, features.NOMINAL, features.NOMINAL, features.NOMINAL,
                    features.NUMERIC, features.NUMERIC, features.NOMINAL]

csv_to_data_frame('MLTestData/ml/CSVLoaderTest.csv', True, csv_columns_type)





